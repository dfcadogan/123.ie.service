﻿using _123.ie.Service.Controllers;
using _123_Customers.Core.Repositories;
using _123.ie.Service.Core.Entities;
using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using NUnit.Framework;
using Moq;


/// <summary>
///  ************************************ Only implmented (some not all) unit tests for Get and Create methods ************************************
/// </summary>
namespace _123.ie.Service.Tests.Controllers
{

    [TestFixture]
    public class CustomerControllerTests
    {
        private Mock<ICustomerRepository> _repository;
        private CustomerController _controller;

        private List<Customer> _customers;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<ICustomerRepository>();
            _controller = new CustomerController(_repository.Object);

            _customers = CreateTestCustomers();
        }

        [Test]
        public void Get_CustomerDoesNotExist_ReturnsNotFound()
        {
            _repository
                .Setup(c => c.Get(It.IsAny<int>()))
                .Returns(() => null);

            var result = _controller.GetCustomer(It.IsAny<int>());

            Assert.That(result, Is.TypeOf<NotFoundResult>());
        }


        [Test]
        public void Get_CustomerExists_ReturnOk()
        {
            _repository
                .Setup(c => c.Get(_customers.First().Id))
                .Returns(_customers.First());

            var result = _controller.GetCustomer(_customers.First().Id);

            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<Customer>>());
            Assert.That((result as OkNegotiatedContentResult<Customer>).Content.Id, Is.EqualTo(_customers.First().Id));
        }

        [Test]
        public void GetAll_CustomersDoNotExist_ReturnsNotFound()
        {
            _repository
                .Setup(c => c.GetAll())
                .Returns(() => null);

            var result = _controller.GetCustomers();

            Assert.That(result, Is.TypeOf<NotFoundResult>());
        }

        [Test]
        public void GetAll_CustomersExist_ReturnsOk()
        {
            _repository
                .Setup(c => c.GetAll())
                .Returns(_customers);

            var result = _controller.GetCustomers();

            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<IEnumerable<Customer>>>());

            var customers = result as OkNegotiatedContentResult<IEnumerable<Customer>>;

            Assert.That(customers.Content.Count(), Is.EqualTo(_customers.Count));
        }

        [Test]
        public void Create_CustomerIsNull_ReturnsBadResult()
        {
            var result = _controller.CreateCustomer(null);

            Assert.That(result, Is.TypeOf<BadRequestResult>());
        }

        [Test]
        public void Create_NotCreated_ReturnsBadResult()
        {
            _repository
                .Setup(c => c.Create(_customers.First()))
                .Returns(() => null);

            var result = _controller.CreateCustomer(_customers.First());

            Assert.That(result, Is.TypeOf<BadRequestResult>());
        }

        [Test]
        public void Create_ModelStateIsInvalid_ReturnsInvalidModelStateResult()
        {
            _controller.ModelState.AddModelError("", "");

            var result = _controller.CreateCustomer(_customers.First());

            Assert.That(result, Is.TypeOf<InvalidModelStateResult>());
        }

        [Test]
        public void CreateCustomerCreated_ReturnsOkResult()
        {
            _repository
                .Setup(c => c.Create(_customers.First()))
                .Returns(_customers.First());

            _repository
                .Setup(c => c.Commit())
                .Returns(_customers.First().Id);

            _controller.Request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost/api/customers")
            };

            var result = _controller.CreateCustomer(_customers.First());

            Assert.That(result, Is.TypeOf<CreatedNegotiatedContentResult<Customer>>());

        }

        private static List<Customer> CreateTestCustomers()
        {
            return new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Name = "Dave",
                    Phone = "12345666"
                },
                new Customer
                {
                    Id = 2,
                    Name = "Dave 2",
                    Phone = "1221212"
                }

            };
        }
    }
}
