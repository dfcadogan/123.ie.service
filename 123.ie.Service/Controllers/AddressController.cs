﻿using _123.ie.Service.Core.Entities;
using _123_Customers.Core.Repositories;
using System;
using System.Web.Http;

namespace _123.ie.Service.Controllers
{
    [RoutePrefix("api/addresses")]
    public class AddressController : ApiController
    {
        private readonly IAddressRepository _repository;

        public AddressController(IAddressRepository repository)
        {
            _repository = repository;
        }

        [HttpGet, Route("")]
        public IHttpActionResult Get()
        {
            var addresses = _repository.GetAll();

            if (addresses == null)
                return NotFound();

            return Ok(addresses);
        }

        [HttpGet, Route("{id:int}")]
        public IHttpActionResult Get(int id)
        {
            var address = _repository.Get(id);

            if (address == null)
                return NotFound();

            return Ok(address);
        }

        [HttpPost, Route("")]
        public IHttpActionResult CreateAddress([FromBody]Address address)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                if (_repository.Create(address) == null)
                    return BadRequest();

                _repository.Commit();

                return Created(new Uri(Request.RequestUri + "/" + address.Id), address);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPut, Route("{id:int}")]
        public IHttpActionResult Put(int id, [FromBody]Address address)
        {
            if (!ModelState.IsValid || address == null)
                return BadRequest(ModelState);

            var addressInDb = _repository.Get(id);

            if (addressInDb == null)
                return NotFound();

            MapAddressProperties(addressInDb, address);

            try
            {
                _repository.Update(addressInDb);
                _repository.Commit();

                return Ok(address);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete, Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            if (_repository.Get(id) == null)
                return NotFound();

            try
            {
                _repository.Delete(id);
                _repository.Commit();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private void MapAddressProperties(Address addressInDb, Address address)
        {
            addressInDb.Street = address.Street;
            addressInDb.Town = address.Town;
            addressInDb.EirCode = address.EirCode;
        }

    }
}
