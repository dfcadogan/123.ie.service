﻿using _123.ie.Service.Core.Entities;
using System.Web.Http;
using _123_Customers.Core.Repositories;
using System;

namespace _123.ie.Service.Controllers
{
    [RoutePrefix("api/Customers")]
    public class CustomerController : ApiController
    {
        private readonly ICustomerRepository _repository;

        public CustomerController(ICustomerRepository repository)
        {
            _repository = repository;
        }

        [HttpGet, Route("")]
        public IHttpActionResult GetCustomers()
        {
            var customers = _repository.GetAll();

            if (customers == null)
                return NotFound();

            return Ok(customers);
        }

        [HttpGet, Route("{id:int}")]
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _repository.Get(id);

            if (customer == null)
                return NotFound();

            return Ok(customer);
        }


        [HttpPost, Route("")]
        public IHttpActionResult CreateCustomer([FromBody]Customer customer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                if (_repository.Create(customer) == null)
                    return BadRequest();

                _repository.Commit();
                return Created(new Uri(Request.RequestUri + "/" + customer.Id), customer);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut, Route("{id:int}")]
        public IHttpActionResult UpdateCustomer(int id, [FromBody]Customer customer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customerInDb = _repository.Get(id);

            if (customerInDb == null)
                return NotFound();

            MapCustomerProperties(customerInDb, customer);

            try
            {
                _repository.Update(customerInDb);
                _repository.Commit();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(customer);
        }

        [HttpDelete, Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            if (_repository.Get(id) == null)
                return BadRequest();

            try
            {
                _repository.Delete(id);
                _repository.Commit();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // Should use DTOs and a convention mapping tool like AutoMapper ...
        private void MapCustomerProperties(Customer to, Customer from)
        {
            to.Name = from.Name;
            to.Phone = from.Phone;
            to.AddressId = from.AddressId;
        }
    }
}
