﻿using _123.ie.Service.Core.Entities;
using _123_Customers.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _123.ie.Service.Persistence
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ApplicationDbContext _context;

        public CustomerRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public Customer Create(Customer customer)
        {
            return _context.Customers.Add(customer);
        }

        public void Delete(int id)
        {
            var customer = _context.Customers.Find(id);

            if (customer != null)
                _context.Customers.Remove(customer);
        }

        public Customer Get(int id)
        {
            return _context.Customers
                .SingleOrDefault(c => c.Id == id);
        }

        public IEnumerable<Customer> GetAll()
        {
            return _context.Customers
                .ToList();
        }

        public Customer Update(Customer customer)
        {
            _context.Entry(customer).State = EntityState.Modified;

            return customer;
        }
    }
}