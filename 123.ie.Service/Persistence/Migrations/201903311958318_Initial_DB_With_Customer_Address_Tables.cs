namespace _123.ie.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_DB_With_Customer_Address_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Town = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        EirCode = c.String(nullable: false, maxLength: 7, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Phone = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        AddressId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId)
                .Index(t => t.AddressId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Customers", new[] { "AddressId" });
            DropTable("dbo.Customers");
            DropTable("dbo.Addresses");
        }
    }
}
