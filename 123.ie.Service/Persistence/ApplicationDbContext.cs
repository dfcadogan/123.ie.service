﻿using _123.ie.Service.Core.Entities;
using MySql.Data.Entity;
using System.Data.Entity;

namespace _123.ie.Service.Persistence
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Address> Addresses { get; set; }

        public ApplicationDbContext() : base("DefaultConnection")
        {

        }
    }
}