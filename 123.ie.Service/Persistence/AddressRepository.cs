﻿using _123.ie.Service.Core.Entities;
using _123_Customers.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _123.ie.Service.Persistence
{
    public class AddressRepository : IAddressRepository
    {
        private readonly ApplicationDbContext _context;

        public AddressRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public Address Create(Address address)
        {
            return _context.Addresses.Add(address);
        }
        public void Delete(int id)
        {
            var address = _context.Addresses.Find(id);

            if (address != null)
                _context.Addresses.Remove(address);
        }

        public Address Get(int id)
        {
            return _context.Addresses
                .SingleOrDefault(a => a.Id == id);
        }

        public IEnumerable<Address> GetAll()
        {
            return _context.Addresses
                .ToList();
        }

        public Address Update(Address address)
        {
            _context.Entry(address).State = EntityState.Modified;

            return address;
        }
    }
}