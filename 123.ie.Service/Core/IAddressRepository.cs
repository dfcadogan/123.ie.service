﻿using _123.ie.Service.Core.Entities;
using System.Collections.Generic;

namespace _123_Customers.Core.Repositories
{
    // Could use a generic repository for this and Customer ... IRepository<T> etc.
    public interface IAddressRepository
    {
        Address Get(int id);
        IEnumerable<Address> GetAll();
        Address Create(Address customer);
        Address Update(Address customer);
        void Delete(int id);
        int Commit();
    }
}


