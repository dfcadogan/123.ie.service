﻿using System.ComponentModel.DataAnnotations;

namespace _123.ie.Service.Core.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        public string Phone { get; set; }

        public Address Address { get; set; } // Navigation property
        public int? AddressId { get; set; }

    }
}