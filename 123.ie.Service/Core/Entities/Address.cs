﻿using System.ComponentModel.DataAnnotations;

namespace _123.ie.Service.Core.Entities
{
    public class Address
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Street { get; set; }
        [Required]
        [MaxLength(100)]
        public string Town { get; set; }

        [Required]
        [MaxLength(7)]
        public string EirCode { get; set; }
    }
}