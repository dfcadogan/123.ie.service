﻿using _123.ie.Service.Core.Entities;
using System.Collections.Generic;

namespace _123_Customers.Core.Repositories
{
    /// <summary>
    /// This is not the "correct" repository pattern as such ...
    /// </summary>
    public interface ICustomerRepository
    {
        Customer Get(int id);
        IEnumerable<Customer> GetAll();
        Customer Create(Customer customer);
        Customer Update(Customer customer);
        void Delete(int id);
        int Commit();
    }
}


